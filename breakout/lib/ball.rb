require "./racket"

class Ball
  attr_reader :pos_col, :pos_row, :agle, :direction

  DIRECTIONS = {
    :up         => "up",
    :down       => "down",
    :right_up   => "right_up",
    :left_up    => "left_up",
    :rigth_down => "rigth_down",
    :left_down  => "left_down"
  }

  def initialize(row, col, direction)
    @pos_row   = row.to_i
    @pos_col   = col.to_i
    @direction = DIRECTIONS.fetch(direction, :right_up)
  end

  def initialize(racket)
    calculate_initial_pos(racket)
    @direction = DIRECTIONS[:right_up]
  end

  def move(game)
    case @direction
      when DIRECTIONS[:up]
        next_pos_row = @pos_row - 1
        next_pos_col = @pos_col

      when DIRECTIONS[:down]
        next_pos_row = @pos_row + 1
        next_pos_col = @pos_col

      when DIRECTIONS[:right_up]
        next_pos_row = @pos_row - 1
        next_pos_col = @pos_col + 1

      when DIRECTIONS[:right_down]
        next_pos_row = @pos_row + 1
        next_pos_col = @pos_col + 1

      when DIRECTIONS[:left_up]
        next_pos_row = @pos_row - 1
        next_pos_col = @pos_col - 1

      when DIRECTIONS[:left_down]
        next_pos_row = @pos_row + 1
        next_pos_col = @pos_col - 1
    end

    perform_movement(next_pos_row, next_pos_col, game)
  end

  def is_on_board?(rows, cols)
    @pos_row <= rows - 1 && @pos_row >= 0 && @pos_col >= 0 && @pos_col <= cols -1
  end

  private

  def perform_movement(next_pos_row, next_pos_col, game)
    cols   = game.cols
    rows   = game.rows
    racket = game.racket

    if is_right_boundary?(next_pos_col, cols)
      next_pos_col = @pos_col - 1
      @direction   = (@direction == DIRECTIONS[:right_up] ? DIRECTIONS[:left_up] :
                                                            DIRECTIONS[:left_down])
    elsif is_left_boundary?(next_pos_col, cols)
      next_pos_col = @pos_col + 1
      @direction   = (@direction == DIRECTIONS[:left_up] ? DIRECTIONS[:right_up] :
                                                           DIRECTIONS[:right_down])
    elsif is_top_boundary?(next_pos_row, next_pos_col, game.blocks)
      hash_key = [next_pos_row, next_pos_col]
      game.board[next_pos_row][next_pos_col] = false
      game.score += game.blocks[hash_key].mvp if game.blocks.has_key?(hash_key)
      game.blocks.delete(hash_key)
      next_pos_row = @pos_row + 1

      if @direction == DIRECTIONS[:up]
        @direction = DIRECTIONS[:down]
      elsif @direction == DIRECTIONS[:right_up]
        @direction = DIRECTIONS[:right_down]
      elsif @direction == DIRECTIONS[:left_up]
        @direction = DIRECTIONS[:left_down]
      end

    elsif is_bottom_boundary?(next_pos_row, rows, racket)
      next_pos_row  = @pos_row - 1
      racket_middle = (racket.columns.first + racket.columns.last) / 2

      if @pos_col < racket_middle
        @direction = DIRECTIONS[:left_up]
        next_pos_col = @pos_col - 1
      elsif @pos_col > racket_middle
        @direction = DIRECTIONS[:right_up]
        next_pos_col = @pos_col + 1
      else
        @direction = DIRECTIONS[:up]
        next_pos_col = @pos_col
      end
    end

    @pos_row = next_pos_row
    @pos_col = next_pos_col
  end

  def is_right_boundary?(next_pos_col, cols)
    next_pos_col >= cols
  end

  def is_left_boundary?(next_pos_col, cols)
    next_pos_col < 0
  end

  def is_top_boundary?(next_pos_row, next_pos_col, blocks)
    blocks.has_key?([next_pos_row, next_pos_col]) || next_pos_row < 0
  end

  def is_bottom_boundary?(next_pos_row, rows, racket)
    next_pos_row == rows - 1 && racket.columns.any?{|column| column == @pos_col}
  end

  def calculate_initial_pos(racket)
    @pos_col = racket.pos_col + racket.size / 2
    @pos_row = racket.pos_row - 1
  end
end