class Block
  attr_reader :pos_row, :pow_col, :mvp

  COLORS = ['red', 'yellow', 'blue', 'green', 'light_blue']

  def initialize(row, col, mvp = 1)
    @pos_row = row
    @pow_col = col
    @mvp     = mvp
    @color   = set_random_color
  end

  def print_block
    case @color
      when 'red'
        print "*".red
      when 'yellow'
        print "*".yellow
      when 'blue'
        print "*".blue
      when 'green'
        print "*".green
      when 'light_blue'
        print "*".light_blue
    end
  end

  private

  def set_random_color
    COLORS[rand(0..4)]
  end
end
