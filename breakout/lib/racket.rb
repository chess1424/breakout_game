class Racket
  attr_reader :pos_row, :pos_col, :size

  def initialize(row, col, size = 5)
    @pos_row = row.to_i
    @pos_col = col.to_i
    @size    = size.to_i
  end

  def move_to_right(cols)
    @pos_col += 1 if @pos_col + @size  < cols
  end

  def move_to_left(cols)
    @pos_col -= 1 if @pos_col - 1 >= 0
  end

  def columns
    cols = []
    for col in @pos_col...@pos_col + @size
      cols << col
    end
  end
end