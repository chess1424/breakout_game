require 'io/console'
require 'colorize'
require "./block"
require "./racket"
require "./ball"

module Breakout

  class Game
    attr_reader :rows, :cols, :blocks, :board, :racket, :score
    attr_writer :score

    def initialize(rows, cols, rows_with_blocks)
      @score            = 0
      @rows             = rows
      @cols             = cols
      @blocks           = Hash.new
      @board            = Array.new(rows){ Array.new(cols, false) }
      @racket           = Racket.new(@rows-1, 0)
      @rows_with_blocks = rows_with_blocks

      create_components
    end

    def start_game
      t1 = Thread.new do
        while !game_is_over?
          print_board
          set_ball_pos
          sleep(0.2)
        end

        raise "Congratulations!! You have won the game! your total score was #{@score} points"
      end

      t2 = Thread.new do
        while !game_is_over?
          set_racket_pos
        end
      end

      t1.join
      t2.join

    rescue => exception
      puts exception
    end

    private

    def game_is_over?
      !is_any_block? || !is_any_ball?
    end

    def print_board
      for row in 0...@rows
        for col in 0...@cols
          if @board[row][col]
            block = blocks[[row, col]]
            print is_row_of_blocks?(row) && block ? block.print_block : "o"
          elsif is_cell_for_racket?(row, col)
            print "*"
          else
            print " "
          end
        end

        puts "\r\n"
      end
      puts "SCORE: #{@score} \r\n"
    end

    def set_racket_pos
      key = STDIN.getch

      if key == "m" #move to right
        @racket.move_to_right(@cols)
      elsif key == "n" #move to left
        @racket.move_to_left(@cols)
      end
    end

    def set_ball_pos
      @board[@ball.pos_row][@ball.pos_col] = false
      @ball.move(self)
      @board[@ball.pos_row][@ball.pos_col] = true

    rescue
      raise "UPS!! You have lost!! your total score was #{@score} points"
    end

    def is_row_of_blocks?(row)
      row >= 0 && row < @rows_with_blocks
    end

    def is_cell_for_racket?(row, col)
      row == @rows -1 && @racket.columns.any?{ |column| column == col }
    end

    def create_components
      for row in 0...@rows_with_blocks
        for col in 0...@cols
          hash_key = [row, col]
          @blocks[hash_key] = Block.new(row, col, 2)
          @board[row][col]  = true
        end
      end

      @ball = Ball.new(@racket)
      @board[@ball.pos_row][@ball.pos_col] = true
    end

    def is_any_block?
      @blocks.length > 0
    end

    def is_any_ball?
      @ball.is_on_board?(@rows, @cols)
    end
  end

  game = Game.new(10, 10, 2)
  game.start_game

end
